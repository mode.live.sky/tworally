// Ignore Spelling: Scriptable
using UnityEngine;

namespace TR.Scripts
{
    [CreateAssetMenu(fileName = "_FollowCameraSettings", menuName = "_TwoRally/_FollowCameraSettings", order = 1)]
    public class _FollowCameraScriptable : ScriptableObject
    {
        [Header("Car reference:")]
        public Transform _CarToFollow;

        [Header("Camera settings:")]
        public float _moveSmoothness = 1f;
        public float _rotationSmoothness = 1f;

        public Vector3 _moveOffset;
        public Vector3 _rotationOffset;
    }
}