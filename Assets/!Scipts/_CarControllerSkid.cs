using UnityEngine;

namespace TR.Scripts
{
    public partial class _CarController : MonoBehaviour
    {
        private bool _isSteerSkidding = false;
        private bool _isMoveSkidding = false;

        private void _wheelEffect()
        {
            foreach (_Wheel wheel in this._wheels)
            {
                if (_isSteerSkidding)
                {
                    wheel._WheelEffectGameObject.GetComponentInChildren<TrailRenderer>().emitting = true;
                    continue;
                }
                else if (_isMoveSkidding && wheel._Axel == _Axel._Rear)
                {
                    wheel._WheelEffectGameObject.GetComponentInChildren<TrailRenderer>().emitting = true;
                    continue;
                }

                wheel._WheelEffectGameObject.GetComponentInChildren<TrailRenderer>().emitting = false;
            }
        }
    }
}