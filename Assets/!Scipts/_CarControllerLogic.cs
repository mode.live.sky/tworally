// Ignore Spelling: Collider
using UnityEngine;
using System;
using System.Collections.Generic;

namespace TR.Scripts
{
    public partial class _CarController
    {
        [Space(10), Header("Wheels mesh and collider references:")]
        [SerializeField] private List<_Wheel> _wheels = new();

        private void _moveWheels()
        {
            foreach (_Wheel wheel in this._wheels)
            {
                float torqueToAdd = this._moveInput * 600f * this._maxAcceleration * Time.deltaTime;
                wheel._WheelCollider.motorTorque = torqueToAdd;

                if (torqueToAdd < 50)
                {
                    this._isMoveSkidding = true;
                }
                else
                {
                    this._isMoveSkidding = false;
                }
            }
        }

        private void _steerWheels()
        {
            foreach (_Wheel wheel in this._wheels)
            {
                if (wheel._Axel != _Axel._Front)
                {
                    continue;
                }

                float steerAngle = this._steerInput * this._turnSensitivity * this._maxSteeringAngle;
                float currentSteerAngle = Mathf.Lerp(
                    wheel._WheelCollider.steerAngle, steerAngle, 0.6f);
                wheel._WheelCollider.steerAngle = currentSteerAngle;

                if (Mathf.Abs(steerAngle) > 25f)
                {
                    this._isSteerSkidding = true;
                }
                else
                {
                    this._isSteerSkidding = false;
                }
            }
        }

        private void _animateWheels()
        {
            foreach (_Wheel wheel in this._wheels)
            {
                wheel._WheelCollider.GetWorldPose(
                    out Vector3 wheelPosition,
                    out Quaternion wheelRotation);

                wheel._WheelTransform.SetPositionAndRotation(wheelPosition, wheelRotation);
            }
        }

    }

    public enum _Axel
    {
        _Rear,
        _Front
    }

    [Serializable]
    public struct _Wheel
    {
        public Transform _WheelTransform;
        public WheelCollider _WheelCollider;
        public GameObject _WheelEffectGameObject;
        public _Axel _Axel;
    }
}