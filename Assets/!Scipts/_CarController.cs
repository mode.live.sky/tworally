using UnityEngine;

namespace TR.Scripts
{
    public partial class _CarController : MonoBehaviour
    {
        [Header("Car basic stats:")]
        [SerializeField] private float _maxAcceleration = 30f;

        [Header("Steering stats:")]
        [SerializeField] private float _turnSensitivity = 1.0f;
        [SerializeField] private float _maxSteeringAngle = 30f;

        [Header("Steering axis names:")]
        [SerializeField] private string _vertical = "Vertical";
        [SerializeField] private string _horizonstal = "Horizontal";

        [Header("Other stats:")]
        [SerializeField] private Vector3 _centerOfMass = new();

        private Rigidbody _carRigidbody;

        private float _moveInput;
        private float _steerInput;
        
        private void Awake()
        {
            this._carRigidbody = this.GetComponent<Rigidbody>();
            this._carRigidbody.centerOfMass = this._centerOfMass;
            this._getCarStartPositionAndRotation();
        }

        private void Update()
        {
            this._getInput();
            this._animateWheels();
            this._wheelEffect();
        }

        private void _getInput()
        {
            this._moveInput = Input.GetAxis(_vertical);
            this._steerInput = Input.GetAxis(_horizonstal);
        }

        private void LateUpdate()
        {
            this._moveWheels();
            this._steerWheels();
        }

    }
}