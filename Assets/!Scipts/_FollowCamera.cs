using UnityEngine;

namespace TR.Scripts
{
    public class _FollowCamera : MonoBehaviour
    {
        [Header("Car reference:")]
        [SerializeField] private Transform _carToFollow;

        [Header("Camera settings:")]
        [SerializeField] private float _moveSmoothness = 1f;
        [SerializeField] private float _rotationSmoothness = 1f;

        [SerializeField] private Vector3 _moveOffset;
        [SerializeField] private Vector3 _rotationOffset;

        private void FixedUpdate()
        {
            this._FollowTarget();
        }

        private void _FollowTarget()
        {
            this._HandleCameraMove();
            this._HandleCameraRotation();
        }

        private void _HandleCameraMove()
        {
            Vector3 targetPosition = new Vector3();
            targetPosition = this._carToFollow.TransformPoint(_moveOffset);

            this.transform.position = Vector3.Lerp(
                this.transform.position,
                targetPosition,
                this._moveSmoothness * Time.deltaTime);
        }

        private void _HandleCameraRotation()
        {
            Vector3 direction = this._carToFollow.position - this.transform.position;
            Quaternion rotation = new Quaternion();

            rotation = Quaternion.LookRotation(
                direction + _rotationOffset, 
                Vector3.up);

            this.transform.rotation = Quaternion.Lerp(
                this.transform.rotation,
                rotation,
                _rotationSmoothness * Time.deltaTime);
        }
    }
}