using UnityEngine;

namespace TR.Scripts
{
    public partial class _CarController : MonoBehaviour
    {
        [Header("Re-spawn tag:")]
        [SerializeField] private string _respawnAreaTag;

        private Vector3 _startCarPosition;
        private Quaternion _startCarRotation;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(this._respawnAreaTag))
            {
                this.transform.SetPositionAndRotation(this._startCarPosition, this._startCarRotation);
            }
        }

        private void _getCarStartPositionAndRotation()
        {
            this._startCarPosition = this.transform.position;
            this._startCarRotation = this.transform.rotation;
        }
    }
}